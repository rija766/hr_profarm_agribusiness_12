# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, date
from dateutil.relativedelta import relativedelta
import xlsxwriter
import calendar
from io import BytesIO
import base64

class inventories_excel_extended(models.Model):
    _name= "excel.extended"

    excel_file = fields.Binary('Cliquer sur le lien pour télécharger')

    file_name = fields.Char('Excel File', size=64)


class Etat_de_paie(models.Model):
    _name = "etat.paie.excel"

    lot_id = fields.Many2one('hr.payslip.run', string='Lot')

    @api.multi
    def extract_declaration_etat_paie(self):
        shortname = 'etat_de_paie_' + datetime.now().strftime('%Y_%m_%d_%H_%M_%S') + '.xlsx'
        # workbook_name = '/home/dany/Documents/binary/' + shortname
        workbook_name = '/opt/binary/' + shortname

        file_data = BytesIO()


        # workbook = xlsxwriter.Workbook(workbook_name)
        workbook = xlsxwriter.Workbook(file_data)

        worksheet = workbook.add_worksheet()
        bold = workbook.add_format({'bold': True})
        date_format = workbook.add_format({'num_format': 'dd-mm-yyyy hh:MM:ss'})
        #worksheet.set_column(0, 34, 10)
        worksheet.set_column('C:C', 40)
        worksheet.set_column('D:D', 15)
        worksheet.set_column('A:B', 8)
        worksheet.set_column('E:AJ', 15)

        worksheet.set_column('AK:AL', 30)


        row = 0
        col = 0

        merge_format = workbook.add_format({
                'border': 1,
                'align': 'center',
                'font_name' : 'Arial',
                'font_size': 10,
                'valign': 'vcenter',
            })

        name_format = workbook.add_format({
                'border': 1,
                # 'align': 'center',
                'font_name' : 'Arial',
                'font_size': 10,
                'valign': 'vcenter',
            })
        merge_format.set_text_wrap()
        merge_format.set_shrink()
        

        worksheet.merge_range('E2:X2','ETAT NOMINATIF DES TRAITEMENTS, SALAIRES ET ASSIMILES',merge_format)
        # worksheet.merge_range('U2:V2','Mois de '+self.mois_id.name,merge_format)
        worksheet.merge_range('A6:A8', 'NOM ET PRENOMS DU TRAVAILLEUR', merge_format)
        worksheet.merge_range('B6:B8', 'Profession', merge_format)
        worksheet.merge_range('C6:C8', 'Adresse ou domicile', merge_format)
        worksheet.merge_range('D6:D8', 'Salire de Base', merge_format)
        worksheet.merge_range('E6:E8', 'CONGÉS SANS SOLDE', merge_format)
        worksheet.merge_range('F6:F8', 'PRÉAVIS payé', merge_format)
        worksheet.merge_range('G6:G8', 'CONGES PAYES', merge_format)
        worksheet.merge_range('H6:H8', 'SALAIRE BRUT', merge_format)
        worksheet.merge_range('I6:I8', 'CNAPS', merge_format)
        worksheet.merge_range('J6:J8', 'OSTIE', merge_format)
        worksheet.merge_range('K6:K8', 'REVENU IMPOSABLE', merge_format)
        worksheet.merge_range('L6:L8', 'Impôt brut', merge_format)
        worksheet.merge_range('M6:M8', 'Nombre de personne à charge', merge_format)
        worksheet.merge_range('N6:N8', 'Réduction pour personne à cha', merge_format)
        worksheet.merge_range('O6:O8', 'Impôt net', merge_format)
        worksheet.merge_range('P6:P8', 'Salaire net', merge_format)
        worksheet.merge_range('Q6:Q8', 'Salaire net arrondi', merge_format)
        worksheet.merge_range('R6:R8', 'Avance/AUTRES', merge_format)
        worksheet.merge_range('S6:S8', 'Préavis Retenu', merge_format)
        worksheet.merge_range('T6:T8', 'Net à payer', merge_format)
        worksheet.merge_range('U6:U8', 'Nouvel arrondi', merge_format)     
        worksheet.merge_range('V6:V8', 'Mode Paiement', merge_format)
        worksheet.merge_range('W6:W8', 'Numéro de Compte Bancaire', merge_format)
        worksheet.merge_range('X6:X8', 'Nom banque', merge_format)
        #worksheet.freeze_panes(6,0)
        #worksheet.freeze_panes(7,0)
        worksheet.freeze_panes(8,col+1)
        worksheet.freeze_panes(8,col+2)
        worksheet.freeze_panes(8,col+3)
        worksheet.freeze_panes(8,col+4)

        

        row=8
        last_row = 0
        total_salaire_base = 0
        total_salaire_mois = 0
        total_cconge_sssolde= 0
        total_all_conge = 0
        total_cp = 0
        total_rem_brut = 0
        total_cnaps = 0
        total_funhec = 0
        total_imposable = 0
        total_irsa_brut = 0
        total_enf = 0
        total_ded_enfant = 0
        total_irsa = 0
        total_net = 0
        total_net_arrondi = 0
        total_avance = 0
        total_preavis_ret = 0
        total_ecart_arr = 0
        total_netp = 0



        # num=1
        chiffres= workbook.add_format({
                'border': 1,
                'align': 'right',
                'font_name' : 'Arial',
                'font_size': 10,
                'valign': 'vcenter',
                'num_format': '# ### ##0.00',
        })
        payslips = self.env['hr.payslip'].search([('payslip_run_id', '=', self.lot_id.id)])


        for payslip in payslips:
            
            # heures_supp = sum([x.amount for x in payslip.line_ids if x.code in ['HS30', 'HS50', 'HSJF', 'HMDIM', 'HMNUIT']])
            worksheet.write(row, col, payslip.employee_id.name.upper(), merge_format)
                   
            # num+=1
            worksheet.write(row, col+1, payslip.contract_id.job_id.name, merge_format)
            worksheet.write(row, col + 2, payslip.employee_id.address_home_id.name, name_format)
            SB = sum([x.amount for x in payslip.line_ids if x.code in ['SB']])
            worksheet.write(row, col + 3, SB, chiffres)
            

            ALLOC_CONGE_SSOLDE = sum([x.amount for x in payslip.line_ids if x.code in ['ALLOC_CONGE_SSOLDE']])
            worksheet.write(row, col + 4, ALLOC_CONGE_SSOLDE, chiffres)
            CP = sum([x.amount for x in payslip.line_ids if x.code in ['CP']])
            worksheet.write(row, col + 5, CP, chiffres)
            ALLOC_CONGE = sum([x.amount for x in payslip.line_ids if x.code in ['ALLOC_CONGE']])
            worksheet.write(row, col + 6, ALLOC_CONGE, chiffres)
            BRUT = sum([x.amount for x in payslip.line_ids if x.code in ['BRUT']])
            worksheet.write(row, col + 7,BRUT, chiffres)
            CNAPS = sum([x.amount for x in payslip.line_ids if x.code in ['CNAPS_EMP']])
            worksheet.write(row,col + 8,CNAPS, chiffres)
            OSTIE = sum([x.amount for x in payslip.line_ids if x.code in ['FUNHECE']])
            worksheet.write(row,col+9,OSTIE, chiffres)
            IMPOSABLE = sum([x.amount for x in payslip.line_ids if x.code in ['IMPOSABLE']])
            worksheet.write(row,col+10,IMPOSABLE, chiffres)
            IRSA = sum([x.amount for x in payslip.line_ids if x.code in ['IRSA']])
            DED_ENFANT = sum([x.amount for x in payslip.line_ids if x.code in ['DED_ENFANT']])
            IRSA_BRUTE = IRSA + DED_ENFANT
            worksheet.write(row,col+11,IRSA_BRUTE, chiffres)
            worksheet.write(row,col+12,payslip.contract_id.children_lt_21, chiffres)
            worksheet.write(row,col+13,DED_ENFANT, chiffres)

            worksheet.write(row,col+14,IRSA, chiffres)
            NET = sum([x.amount for x in payslip.line_ids if x.code in ['NET']])
            worksheet.write(row,col+15, NET , chiffres)
            ECART_ARRONDI = sum([x.amount for x in payslip.line_ids if x.code in ['ECART_ARRONDI']])
            worksheet.write(row,col+16,(NET - ECART_ARRONDI), chiffres)
            AVANCE = sum([x.amount for x in payslip.line_ids if x.code in ['AVANCE15']])
            worksheet.write(row,col+17,AVANCE, chiffres)
            PREAVIS = sum([x.amount for x in payslip.line_ids if x.code in ['PREAVIS']])
            worksheet.write(row,col+18,PREAVIS, chiffres)
            NETAPAYER = sum([x.amount for x in payslip.line_ids if x.code in ['NETAPAYER']])
            worksheet.write(row,col+19,NETAPAYER, chiffres)
            
            worksheet.write(row,col+20,ECART_ARRONDI, chiffres)
            worksheet.write(row,col+21,payslip.contract_id.payslip_payment_mode_id.name, chiffres)

            worksheet.write(row,col+22,payslip.employee_id.bank_account_id.acc_number, name_format)
            worksheet.write(row,col+23,payslip.employee_id.bank_account_id.bank_id.name, name_format)

            total_salaire_base+=payslip.contract_id.wage
            total_salaire_mois+=SB
            total_cconge_sssolde+= ALLOC_CONGE_SSOLDE
            total_all_conge += ALLOC_CONGE
            total_cp += CP
          
            total_rem_brut+=BRUT
            total_cnaps+=CNAPS
            total_funhec+=OSTIE
            total_imposable+=IMPOSABLE
            total_irsa_brut+=IRSA_BRUTE
            total_enf += payslip.contract_id.children_lt_21
            total_ded_enfant +=DED_ENFANT
            total_irsa+=IRSA
            total_net+=NET
            total_net_arrondi += NET - ECART_ARRONDI
            total_avance+=AVANCE
            total_preavis_ret+=PREAVIS
            total_ecart_arr+=ECART_ARRONDI
            total_netp+=NETAPAYER
        #     last_row = row
            row+=1
        
        merge_format2 = workbook.add_format({
            'border': 1,
            'align': 'right',
            'font_name' : 'Arial',
            'font_size': 10,
            'valign': 'vcenter',
            'num_format': '# ### ##0.00',
        })
        merge_format2.set_bold()
        # merge_format2.set_num_format('00')
        row+=1  
        worksheet.write(row, col + 2, 'TOTAL', merge_format2)
        worksheet.write(row, col + 3, total_salaire_base, merge_format2)
        worksheet.write(row, col + 4, total_cconge_sssolde,merge_format2)
        worksheet.write(row, col + 5, total_cp,merge_format2)

        
        worksheet.write(row, col + 6, total_all_conge,merge_format2)
        worksheet.write(row, col + 7, total_rem_brut,merge_format2)
        worksheet.write(row, col + 8, total_cnaps,merge_format2)
        worksheet.write(row, col + 9, total_funhec,merge_format2)
        worksheet.write(row, col + 10, total_imposable,merge_format2)

        worksheet.write(row, col + 11, total_irsa_brut,merge_format2)
        worksheet.write(row, col + 12, total_enf,merge_format2)

        worksheet.write(row, col + 13, total_ded_enfant,merge_format2)
        worksheet.write(row, col + 14, total_irsa,merge_format2)

        worksheet.write(row, col + 15, total_net,merge_format2)
        worksheet.write(row, col + 16, total_net_arrondi, merge_format2)
        # worksheet.write(row, col + 17, total_enf,merge_format2)
        worksheet.write(row, col + 17, total_avance, merge_format2)
        worksheet.write(row, col + 18, total_preavis_ret, merge_format2)
        worksheet.write(row, col + 19, total_ecart_arr, merge_format2)
        worksheet.write(row, col + 20, total_netp, merge_format2)
        worksheet.write(row, col + 21, '-', merge_format2)
        worksheet.write(row, col + 22, '-', merge_format2)
        worksheet.write(row, col + 23, '-',merge_format2)


        workbook.close()

        file_data.seek(0)

        attachment_id = self.env['excel.extended'].create({
            'file_name': shortname, 
            'excel_file':base64.b64encode( file_data.read())
        })

        return {
             'view_mode': 'form',

            'res_id': attachment_id.id,

            'res_model': 'excel.extended',

            'view_type': 'form',

            'type': 'ir.actions.act_window',

            'context': None,

            'target': 'new',
        }
