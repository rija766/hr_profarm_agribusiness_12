# -*- coding: utf-8 -*-

from odoo import models, api, fields
import odoo.addons.decimal_precision as dp
import unicodedata,re

Payroll = dp.get_precision('Payroll')

class res_company(models.Model):
    _inherit = 'res.company'

    igr_threshold = fields.Float(string='Seuil IGR', digits_compute=Payroll)
    igr_threshold1 = fields.Float(string='Seuil IGR1', digits_compute=Payroll)
    igr_threshold2 = fields.Float(string='Seuil IGR2', digits_compute=Payroll)
    igr_threshold3 = fields.Float(string='Seuil IGR3', digits_compute=Payroll)
    igr_rate = fields.Float(string='Taux IGR (%)', digits_compute=Payroll)
    igr_abatment = fields.Float(string='Abattement IGR', digits_compute=Payroll)
    enfant_abatment = fields.Float(string='Abattement Enfant', digits_compute=Payroll, default=2000)
    cnaps_employer_contribution = fields.Float(string='CNAPS employer contribution (%)',
                                         digits_compute=Payroll)
    cnaps_employee_contribution = fields.Float(string='CNAPS employee contribution (%)',
                                        digits_compute=Payroll)
    cnaps_ceiling = fields.Float(string='CNAPS ceiling',
                                 digits_compute=Payroll)
    family_allocation_amount = fields.Float(string='Family allocation amount',
                                  digits_compute=Payroll)
    monthly_leave = fields.Float(string='Monthly leave',
                              digits_compute=Payroll)                                                              
    nif = fields.Char(string='NIF')
    stat = fields.Char(string='STAT')
    cif = fields.Char(string='CIF')
    capital = fields.Float(string='Capital')
    plafond_ostie = fields.Float(
        'Plafond de la Sécurite Sociale',
        digits_compute=dp.get_precision('Payroll'))
    num_cnaps_patr = fields.Char('N° CNAPS', size=64)

    augmentation_annuel = fields.Float(string='Augmentation annuel salaire en (%)')   
    compte_bni = fields.Char(string="RIB BNI")
    compte_boa = fields.Char(string="RIB BOA")
    compte_bfv = fields.Char(string="RIB BFV")
