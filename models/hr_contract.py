# -*- coding: utf-8 -*-

from odoo import models, api, fields
from odoo.exceptions import UserError
from datetime import datetime
from odoo.tools.translate import _

class hr_payslip(models.Model):
    _inherit = 'hr.payslip'
    line_idxs = fields.One2many('hr.payslip.line', 'slip_id', string='Payslip Lines', readonly=True,
        states={'draft': [('readonly', False)]},domain=['&', ('appears_on_payslip', '=', True)])
    soldetoutcompte = fields.Boolean(string='SOLDE DE TOUT COMPTE')
    conge_prises = fields.Float(string='Congés prises')
    nb_leave_solde = fields.Float(string="Solde de congé", compute="_get_nb_leaves_solde", store=True)

    @api.onchange('struct_id')
    def onchange_struct_id(self):
        lines = []
        sequence_index = 0
        for record in self.struct_id.rule_ids:
            for input in record.input_ids:
                print(input.code)

                inputs = {
                 'name': input.name,
                 'code': input.code,
                 'amount': 0,
                 'sequence': sequence_index,
                 'contract_id': self.contract_id.id,
                 # 'payslip_id': self._origin.id

                }
                lines.append(inputs)
                print(inputs)
                # idx=self.env['hr.payslip.input'].create(inputs)
                # self.input_line_ids =idx
                sequence_index += 1


        self.input_line_ids = [(5,)] + [(0, 0, x) for x in lines]
        # self.get_info_compute_sheet()

    @api.multi
    def get_nb_ent(self):
        date_from = (datetime.strptime(str(self.date_from), "%Y-%m-%d")).strftime('%Y-%m-%d %H:%M:%S')
        date_to = (datetime.strptime(str(self.date_to), "%Y-%m-%d")).strftime('%Y-%m-%d %H:%M:%S')

        self._cr.execute(""" SELECT COALESCE(SUM(number_of_days),0) FROM hr_leave WHERE holiday_type='remove' AND state='validate' AND holiday_status_id=1 AND employee_id=%s and date_from BETWEEN %s and %s """,(self.employee_id.id,date_from, date_to,))
        value = 0
        if abs(self._cr.fetchone()[0]):
            value = abs(self._cr.fetchone()[0])
        result = (self.employee_id.company_id.monthly_leave - 2.5) - value
        
        return "{0:,.2f}".format(result).replace(',', ' ').replace('.', ',')

    @api.multi
    def get_nb_leaves(self):
        date_from = (datetime.strptime(str(self.date_from), "%Y-%m-%d")).strftime('%Y-%m-%d %H:%M:%S')
        date_to = (datetime.strptime(str(self.date_to), "%Y-%m-%d")).strftime('%Y-%m-%d %H:%M:%S')

        self._cr.execute(""" SELECT COALESCE(SUM(number_of_days),0) FROM hr_leave WHERE holiday_type='remove' AND state='validate' AND holiday_status_id=1 AND employee_id=%s and date_from BETWEEN %s and %s """,(self.employee_id.id,date_from, date_to,))
        result = abs(self._cr.fetchone()[0])
        
        return "{0:,.2f}".format(result).replace(',', ' ').replace('.', ',')

    @api.multi
    def _get_nb_leaves_solde(self):
        for record in self:
            record.nb_leave_solde = record.employee_id.remaining_leaves + record.employee_id.company_id.monthly_leave

            pass

class hr_contract(models.Model):
    _inherit = 'hr.contract'

    medical_organization_id = fields.Many2one('hr.medical.organization', string="Organisation médical")
    contract_monthly_hours_id = fields.Many2one('hr.contract.monthly.hours', string='Volume horaire')
    children_lt_21 = fields.Integer(string=u'Total enfant moins de 21 ans',help=u'Total enfant moins de 21 ans')
    payslip_payment_mode_id = fields.Many2one('hr.payslip.payment.mode', string='Mode de paiement',required=True)
     

class hr_contract_monthly_hours(models.Model): 
    _name = 'hr.contract.monthly.hours'
    _description = 'Records for initiliazing monthly hours data for contract'

    name = fields.Char(string='Nom', required=True)
    time_code = fields.Char(string='Code', required=True)
    number_of_hours = fields.Float(string='Nombre d\'heure', required=True)



class hr_medical_organization(models.Model):
    _name = 'hr.medical.organization'
    _description = 'Manage Company Medical Organization'

    name = fields.Char(string='Nom', size=64)
    ceiling = fields.Float(string='Ceiling')
    wage_rate = fields.Float(string='Wage rate (%)')
    employer_rate = fields.Float('Employer rate (%)')
    company_id = fields.Many2one('res.company', string=u'Company')


class hr_payslip_payment_mode(models.Model):

    _name = 'hr.payslip.payment.mode'
    _description = 'Payment Mode'

    name = fields.Char(string='Name', size=256, required=True, translate=True)
    note = fields.Text(string='Description')
    payslip_payment_mode_ids = fields.One2many('hr.contract',
                                               'payslip_payment_mode_id',
                                               string='Payment Mode')
    is_mobile = fields.Boolean(string='Mobile')