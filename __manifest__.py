# -*- coding: utf-8 -*-
{
    'name': "HR PROFARM ISAGRI",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "José Pascal",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'hr',
    'sequence': 1,
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr',  'hr_holidays', 'hr_payroll', 'hr_contract', 'portal','hr_payroll_account'],
      'installable': True,

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/data_config.xml',
        'data/hr_payroll_data_rule.xml',
        'views/views.xml',
        'views/etat_de_paie.xml',
        'report/hr_payroll_report.xml'


    ],
     'application': True,
    'uninstall_hook': 'uninstall_hook',
}